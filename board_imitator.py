#!/usr/bin/env python
# -*- coding: utf-8 -*-
# имитирует табло
import socket

sock = socket.socket()
sock.connect(('localhost', 33337))
#p = b'REG359772039762845\r1\x07'
p = b'REG359772039751327\r1\x03' #деревня универсиады
b = sock.send(p)
if b:
    print('send: ', b)
while True:
    data = sock.recv(1024)
    print('get:', data)
    if (data[2] == 8):
        t = b'TDSet'
        print('send:',t)
        sock.send(t)
    elif (data[2] == 1):
        t = b'Y\x01\r'
        print('send:',t)
        sock.send(t)
    elif data[2] == 12:
        t = b'SetTemp'
        print('send:',t)
        sock.send(t)
    else:
        print(data)
#b'TD\x0c+08\r' установка температуры
#b'TD\x08\x0c&\x1f\t\x0b\xdf\r' установка времени
#b'TD\x01S\x01\x01\x01\x05\xb95\x00\x04\xc6\xe5\xeb\xe5\xe7\xed\xee\xe4\xee\xe6\xed\xfb\xe9 \xe2\xee\xea\xe7\xe0\xeb - Railway Station\x00\x1bw\r'
#b'TD\x01S\x01\x01\x01\x05  \x00\x04\x00\xff\xff\r' пустая строка