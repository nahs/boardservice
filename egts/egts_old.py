'''
Fork EGTS.h library to Python
'''
import crc

class header:
    '''
    egts.h: egts_header_s
    '''
    # Mandatory part (5 byte)
    PRV = None  # Protocol Version
    SKID = None # Security Key ID
    # Bitfield
    PRF = None  # 2 Prefix
    RTE = None  # 1 Route
    ENA = None  # 2 Encryption Algoritm
    CMP = None  # 1 Compressed
    PR = None   # 2 Priority
    HL = None   # Header Length, up tu HCS
    HE = None   # Hedaer Encoding
    # << begin encoded part (5 byte + 5(?)byte)
    FDL = None  # Frame Data Length
    PID = None  # Packet Identifier
    PT = None   # Packet Type
    # <<< begin optioal part (5 byte)
    PRA = None  # Peer Address, optional, exist when RTE switched on
    RCA = None  # Recipient Arrress, optional, exist when RTE switched on
    TTL = None  # Time To Live,  optional, exist when RTE switched on
    # >>> end optional part
    # >> end encoded part
    HCS = None  # Header Check Sum, mandatory, XOR

class response:
    '''
    egts.h: egts_response_header_t
    '''
    RPID = None # Response Packet ID
    PR = None   # Processing Result

class signature:
    '''
    egts.h: egts_signature_header_t
    '''
    SIGL = None # Signature Length
    SIGD = None # Signature Data, optional ???
    
class result:
    header = None
    error = {'code': None, 'name': None}
    SFRD = None
    
class SFRD:
    def __init__(self):
        self.response = None
        self.signature = None
    
def egts(egts_packet_buf):
    buf = iter(egts_packet_buf)
    offset = 0

    header.PRV = next(buf)
    offset += 1
    if header.PRV != 1:
        result.error = {'code': 128, 'name': 'EGTS_PC_UNS_PROTOCOL', '0': header.PRV}
        return result
    
    header.SKID = next(buf)
    offset += 1
    
    flags = next(buf)
    offset += 1
    header.PRF = (flags & 0b11000000) >> 6
    header.RTE = bool(flags & 0b00100000)
    header.ENA = (flags & 0b00011000) >> 3
    header.CMP = bool(flags & 0b00000100)
    header.PR = flags & 0b00000011
    
    header.HL = next(buf)
    offset += 1
    if not(header.HL == 11 or header.HL == 16):
        result.error = {'code': 131, 'name': 'EGTS_PC_INC_HEADERFORM'}
        
    header.HE = next(buf)
    offset += 1
    header.FDL = next(buf) + next(buf)
    offset += 2
    header.PID = next(buf) + next(buf)
    offset += 2
    header.PT = next(buf)
    offset += 1
    
    if header.RTE:
        header.PRA = next(buf) + next(buf)
        offset += 2
        header.RCA = next(buf) + next(buf)
        offset += 2
        header.TTL = next(buf)
        offset += 1
        
    header.HCS = next(buf)
    offset += 1
    
    header_buffer_withnout_HCS = egts_packet_buf[0:header.HL-1]
    header_checksum = crc.crc8(header_buffer_withnout_HCS)
    if header_checksum != header.HCS:
        result.error = {'code': 137, 'name': 'EGTS_PCHEADERCRC_ERROR'}
        return result
    
    result.header = header
    
    if not header.FDL:
        return result
    
    # SFRD
    SFRD_buf = egts_packet_buf[offset:offset + header.FDL]
    offset += header.FDL
    SFRCS = next(buf) + next(buf)
    offset += 2
    
    SFRD_checksum = crc.crc16(SFRD_buf)
    if SFRD_checksum != SFRCS:
        result.error = {'code': 138, 'name': 'EGTS_PC_DATACRC_ERROR'}
        
    try:
        print(header.PT)
        result.SFRD = {
            # EGTS_PT_RESPONSE
            '0': parse_SFRD_response(egts_packet_buf),
            # EGTS_PT_APPDATA
            '1': parse_SFRD_appdata(egts_packet_buf),
            # EGTS_PT_SIGNET_APPDATA
            '2': parse_SFRD_signet_appdata(egts_packet_buf)
        }[header.PT]
    except:
        result.error = {'code': 132, 'name': 'EGTS_PC_INC_DATAFORM'}
        
    return result

def parse_SFRD_response(buffer):
    offset = 0
    buf = iter(buffer)
    print(1,'EGTS_PT_RESPONSE')
    response.RPID = next(buf) + next(buf)
    offset += 2
    response.PR = next(buf)
    offset += 1
    
    rest_SFRD = buffer[offset:]
    if len(rest_SFRD):
        SFRD = parse_SFRD_appdata(rest_SFRD)
    else:
        SFRD = None
    
    SFRD.response = response
    
    return SFRD

def parse_SFRD_appdata(buf):
    print(2, 'EGTS_PT_APPDATA')

def parse_SFRD_signet_appdata(buffer):
    print(3, 'EGTS_PT_SIGNET_APPDATA')
    offset = 0
    buf = iter(buffer)
    signature.SIGL = next(buf) + next(buf)
    offset += 2
    signature.SIGD = buffer[offset:signature.SIGL]
    offset += signature.SIGL
    
    rest_SFRD = buffer[offset:]
    if len(rest_SFRD):
        SFRD = parse_SFRD_appdata(rest_SFRD)
    else:
        SFRD = None
            
    SFRD.signature = signature
    
    return SFRD

    
    
    
        
    

    
    
    
    
    