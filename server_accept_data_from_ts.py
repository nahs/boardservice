#!/usr/bin/env python
# -*- coding: utf-8 -*-

# this server accept data from ATServer

import socket
from connect_to_mssql import data_execute
from geo import geo
import json
import urllib.request
import datetime

def main():
    
    conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    g = geo()
    db = data_execute()
    ts = db.ts_list
    polyline = db.polyline_list
    try:
        conn.connect(('10.10.1.2', 33337))
        print("--> connect with AT-server")
        #conn.connect(('217.30.253.70', 82))
    #conn.setblocking(0)
    except socket.error as msg:
        print(1, msg)
        conn = None
        
    if conn is not None:

        while True:
            # в скрипте, читающем данные:
            try: 
                data = conn.recv(1024)
                #print('data lenght:', len(data), data.__sizeof__(),'bytes')
                #print(data)
            except socket.error as msg: # данных нет
                print(2, msg)
                break
            else: # данные есть
                ts.add_move(data)
                plid = 1
                pll = polyline.getitem(plid).segment
                
                #enumerate(ts.getkeys())
                tphones = ['79693121814', '79663977783', '79693121776', '79693121873', '79693122004', 
                           '79693121870', '79693121864', '79693121813', '79693121765', '79663977761', 
                           '79693121809', '79663977777', '79663977617', '79693122003', '79663977619', 
                           '79693121802', '79693121992', '79693121763', '79693121889', '79663977629', 
                           '79663977666', '79693121860', '79693121999', '79663977634', '79663977681', 
                           '79693121766', '79693121865', '79693121775', '79663977778', '79693121995', 
                           '79693121803', '79663977674', '79663977765', '79693121866', '79693121871', 
                           '79663977775', '79693121806', '79663977667']
                for i, t2 in enumerate(ts.getkeys()):
                    if t2 in ts.getkeys():
                        
                        tsd = ts.getitem(t2)
                        if tsd.newevent:
                        
                            '''todo: учесть ситуации когда все машины в депо. ИСКЛЮЧЕНИЯ НА ВСЕ!!!!'''
                            s = g.own_segment(pll, [tsd.latitude_end, tsd.longitude_end, tsd.direction, tsd.speed], 'ts')
                            jd = {}                        
                            
                            if s is not None:
                               # if not s['course']:
                               jd.update({
                                                                          
                                  "type": "FeatureCollection",
                                  "features": [
                                    {
                                      "type": "Feature",
                                      "properties": {
                                            "color": "#f00"
                                                     },
                                      "geometry": {
                                        "type": "LineString",
                                        "coordinates": [
                                          [
                                            pll[s['segment_num']].longitude_start,
                                            pll[s['segment_num']].latitude_start
                                          ],
                                          [
                                           pll[s['segment_num']].longitude_end,
                                           pll[s['segment_num']].latitude_end
                                          ]
                                        ]
                                      }
                                    },
                                    {
                                      "type": "Feature",
                                      "properties": {
                                                     "name": str(tsd.description)},
                                      "geometry": {
                                        "type": "Point",
                                        "coordinates": [
                                          tsd.longitude_end,
                                          tsd.latitude_end
                                          
                                        ]
                                      }
                                    },
                                    {
                                      "type": "Feature",
                                      "properties": {},
                                      "geometry": {
                                        "type": "LineString",
                                        "coordinates": [
                                          [
                                           s['y'],
                                           s['x']
                                           ],
                                            [
                                             s['y2'],
                                             s['x2']
                                            ]
                                          ]
                                        
                                      }
                                    }
                                  ]
                                }
                                          
                                )
                               
                               
                               
                               ''' 
                                jd.update({i:{
                                        'segment': {
                                            'x1': pll[s['segment_num']].latitude_start,
                                            'y1': pll[s['segment_num']].longitude_start,
                                            'x2': pll[s['segment_num']].latitude_end,
                                            'y2': pll[s['segment_num']].longitude_end
                                            },
                                        'ts': {
                                           'x': tsd.latitude_end,
                                           'y': tsd.longitude_end
                                           },
                                        'distance': {
                                             'x1': s['x'],
                                             'y1': s['y'],
                                             'x2': s['x2'],
                                             'y2': s['y2']
                                             },
                                        'name': str(tsd.description)
                                        }  
                                      })
                                
                                print(
                                          'L.polyline([[',
                                          pll[s['segment_num']].latitude_start,
                                          ',',pll[s['segment_num']].longitude_start,
                                          '],[',pll[s['segment_num']].latitude_end,
                                          ',',pll[s['segment_num']].longitude_end,']],{color: \'#f00\'}).addTo(map);'
                                          )
                                
                                print('L.circle([',tsd.latitude_end,', ',tsd.longitude_end,'],10,{color:\'#00f\'}).bindPopup(\'',tsd.description, tsd.direction, s['course'], s['f_num'],'\').addTo(map);')
                                
                                print(
                                          'L.polyline([[',s['x'],',',s['y'],'],[',s['x2'],',',s['y2'],']],{color: \'#00f\'}).addTo(map);'
                                          )
                                '''
                            try:
                                jsondata = json.dumps(jd).encode('utf-8')
                                #print(jsondata)
                                senddata = urllib.request.Request('http://test.kazanmetro.ru/map/ajax.php', data=jsondata, 
                                                                  headers={'content-type:': 'application/json'})
                                req = urllib.request.urlopen(senddata)
                                print(req.read())
                            except:
                                print('error')
                
                
        
if __name__ == "__main__":
    main()