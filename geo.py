'''
Created on 5.1.16
@author: nahs
#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
import math
from decimal import Decimal

class geo:
    '''
    @note: Расчет геоданных
    '''
    def __init__(self):
        #super(geo, self).__init__()
        pass
    
   

    def calculate(self, latitude_start, longitude_start, latitude_end, longitude_end):
        '''
        @note: вычисляет азимут отрезка и расстояние двух координат
        @param latitude_start: начальная долгота отрезка
        @param longitude_start: начальная широта отрезка
        @param latitude_end: конечная долгота отрезка
        @param longitude_end: конечная широта отрезка
        @return: list(course - азимут в десятичных градусах, distantion - расстояние в метрах)   
        '''
        #pi - число pi, rad - радиус сферы (Земли)
        rad = 6372795
        #rad = 6378137
        
         
        #координаты двух точек
        llat1 = float(latitude_start)
        llong1 = float(longitude_start)
        llat2 = float(latitude_end)
        llong2 = float(longitude_end)
        if llat1 != llat2 and llong1 != llong2:
            #в радианах
            lat1 = llat1*math.pi/180.0        
            lat2 = llat2*math.pi/180.0
            long1 = llong1*math.pi/180.0
            long2 = llong2*math.pi/180.0
     
            #косинусы и синусы широт и разницы долгот
            cl1 = math.cos(lat1)
            cl2 = math.cos(lat2)
            sl1 = math.sin(lat1)
            sl2 = math.sin(lat2)
            delta = long2 - long1
            cdelta = math.cos(delta)
            sdelta = math.sin(delta)
             
            #вычисления длины большого круга
            y = math.sqrt(math.pow(cl2*sdelta,2)+math.pow(cl1*sl2-sl1*cl2*cdelta,2))
            x = sl1*sl2+cl1*cl2*cdelta
            ad = math.atan2(y,x)
            dist = ad*rad
            
            #вычисление начального азимута
            x = (cl1*sl2) - (sl1*cl2*cdelta)
            y = sdelta*cl2
            z = math.degrees(math.atan(-y/x))
        
            if (x < 0):
                z = z+180.0
             
            z2 = (z+180.0) % 360.0 - 180.0
            z2 = - math.radians(z2)
            anglerad2 = z2 - ((2*math.pi)*math.floor((z2/(2*math.pi))) )
            angledeg = (anglerad2*180.0)/math.pi
             
            return {'course': angledeg, 'distantion': dist}
        else:
            return {'course':0,'distantion':0}
        

    def compare_course(self, segment, point):
        '''
        @note: сравнивает напрвление движения двух азимутов
        @param segment: азимут отрезка
        @param point:   азимут точки
        @return:  True or False
        '''
        a = Decimal(segment)
        e = Decimal(point)
        res = False
        if a >= 0 and a <= 90:
            d = a - 90 + 360
            c = a + 90
            if d < e < 360 or 0 < e < Decimal(c):
                res = True
        elif a > 90 and a < 270:
            d = a - 90
            c = a + 90
            if d < e < 270 or 90 < e < c:
                res = True
        elif a >= 270 and a <= 360:
            d = a - 90
            c = a + 90 - 360
            if d < e < 360 or 0 < e < c:
                res = True
        return bool(res)
    

    def point_in_route(self, px = 0.0, py = 0.0, lx1 = 0.0, ly1 = 0.0, lx2 = 0.0, ly2 = 0.0):
        '''
        @note: вычисляет нахождение точки возле отрезка
        @param px: широта(lat) точки
        @param py: долгота(lon) точки
        @param lx1: широта(lat) отрезка
        @param ly1: долгота(lon) отрезка
        @param lx2: широта(lat) отрезка
        @param ly2: долгота(lon) отрезка
        @return:  0 - если точка принадлежит отрезку 
        '''
        #math.fabs((x-x1)*(y2-y1)-(y-y1)*(x2-x1))#точка воозле прямой      
        return math.fabs(
                        math.sqrt(
                            math.pow((lx1-px),2)+
                            math.pow((ly1-py),2)
                            ) + 
                        math.sqrt(
                            math.pow((lx2-px),2)+
                            math.pow((ly2-py),2)
                            ) - 
                        math.sqrt(
                            math.pow((lx2-lx1),2)+
                            math.pow((ly2-ly1),2)
                        ))
    
    def own_segment(self, segment = list(), coordinate = list(), flag = 'ts'):
        '''
        @note: проверяет принадлежность точки сегементу
        @param segment: список данных сегмента полилинии
        @param coordinate: список данных точки
        @param flag: ts - подвижной состав(значение азимкта), иначе табло (значение азимута тру или фалс) 
        @return: возвращает идентификатор сегмента и расчет расстояния от точки до конца сегментьа 
        '''
        f2 =[]
        x = float(coordinate[0])
        y = float(coordinate[1])
        c = coordinate[2]
        for seg_num, s in enumerate(segment):
            res = None            
            x1 = float(s.latitude_start)
            y1 = float(s.longitude_start)
            x2 = float(s.latitude_end)
            y2 = float(s.longitude_end)
            cs = s.direction
            #print(r, s.latitude_start, s.distantion)
            
            buf = self.point_in_route(x, y, x1, y1, x2, y2)
            #транспорт или табло?
            if flag == 'ts':
                course = self.compare_course(cs, c)
                speed = Decimal(coordinate[3])*Decimal('0.277778') #км\ч в м\с
            elif flag == 'tablo':
                course = c
                speed = 0
                
            #находим самое меньшее значение
            if buf < 0.001:
                f2.append({'buf': buf, 'segment_num': seg_num, 'x': x, 'y': y, 'x1': x1, 'y1': y1, 'x2': x2, 'y2': y2, 'course': course, 'speed': speed})
            #                    0                1              2        3         4        5          6        7              8               9              
        #спорный момент к какому сегенту принадлежит точка
        if len(f2) == 2:
            #все зависит от направления
            '''TODO: Прверить верность для тс. Для Табло ВЕРНО!!'''
            if f2[0]['course']:
                if f2[0]['buf'] < f2[1]['buf']:
                    res = {
                           'segment_num': f2[0]['segment_num'], 
                           'distantion': self.calculate(f2[0]['x'], f2[0]['x'], f2[0]['x1'], f2[0]['y1'])['distantion'],
                           'course': f2[0]['course'],
                           'speed': f2[0]['speed'],
                           'x': f2[0]['x'], 
                           'y': f2[0]['y'], 
                           'x2': f2[0]['x1'], 
                           'y2': f2[0]['y1'],
                           'f_num': 1
                           }
                elif f2[0]['buf'] > f2[1]['buf']:
                    res = {
                           'segment_num': f2[1]['segment_num'],
                           'distantion': self.calculate(f2[1]['x'], f2[1]['y'], f2[1]['x2'], f2[1]['y2'])['distantion'],
                           'course': f2[1]['course'],
                           'speed': f2[1]['speed'],
                           'x': f2[1]['x'], 
                           'y': f2[1]['y'], 
                           'x2': f2[1]['x2'], 
                           'y2': f2[1]['y2'],
                           'f_num': 2
                           }
            else:
                if f2[0]['buf'] < f2[1]['buf']:
                    res = {
                           'segment_num': f2[0]['segment_num'],
                           'distantion': self.calculate(f2[0]['x'], f2[0]['y'],f2[0]['x1'],f2[0]['y1'])['distantion'],
                           'course': f2[0]['course'],
                           'speed': f2[0]['speed'],
                           'x': f2[0]['x'], 
                           'y': f2[0]['y'],
                           'x2': f2[0]['x1'],
                           'y2': f2[0]['y1'],
                           'f_num': 3
                           }
                elif f2[0]['buf'] > f2[1]['buf']:
                    res = {
                           'segment_num': f2[1]['segment_num'],
                           'distantion': self.calculate(f2[1]['x'], f2[1]['y'],f2[1]['x2'], f2[1]['y2'])['distantion'],
                           'course': f2[1]['course'],
                           'speed': f2[1]['speed'],
                           'x': f2[1]['x'], 
                           'y': f2[1]['y'], 
                           'x2': f2[1]['x2'], 
                           'y2': f2[1]['y2'],
                           'f_num': 4
                        }
        elif len(f2)==1:
            if f2[0]['course']:
                res = {
                       'segment_num': f2[0]['segment_num'], 
                       'distantion': self.calculate(f2[0]['x'], f2[0]['y'], f2[0]['x1'], f2[0]['y1'])['distantion'],
                       'course': f2[0]['course'],
                       'speed': f2[0]['speed'],
                       'x': f2[0]['x'], 
                       'y': f2[0]['y'],
                       'x2': f2[0]['x1'],
                       'y2': f2[0]['y1'],
                       'f_num': 5
                    }
            else:
                res = {
                       'segment_num': f2[0]['segment_num'], 
                       'distantion': self.calculate(f2[0]['x'], f2[0]['y'], f2[0]['x2'], f2[0]['y2'])['distantion'],
                       'course': f2[0]['course'],
                       'speed': f2[0]['speed'],
                       'x': f2[0]['x'], 
                       'y': f2[0]['y'],
                       'x2': f2[0]['x2'],
                       'y2': f2[0]['y2'],
                       'f_num': 6
                    }
        elif len(f2)==0:
            res = None
        
        return res
    
    
    
    
    
    
    
    
    