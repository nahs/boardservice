# -*- coding: utf-8 -*-

'''#обработка пакета данных
t1 = ['55.8627510070801', '49.1073341369629', '0', '-1', '79663977636', '16122015:131913', '4', 'tramway', '55.8617973327637', '49.1058731079102', '13', '123', '79663977636', '16122015:132847', '4', 'tramway', '']
t2 = ['55.7605819702148', '49.1906547546387', '20', '315', '79693121862', '17122015:135855', '5', 'tramway', '55.7575759887695', '49.1958694458008', '31', '0', '79693121865', '17122015:135831', '4', 'tramway', '55.7700538635254', '49.2190437316895', '46', '180', '79693121775', '17122015:135919', '5', 'tramway', '55.8353462219238', '49.197093963623', '0', '205', '79663977712', '17122015:135841', '1', 'tramway', '55.7805786132813', '49.2192573547363', '37', '0', '79693121777', '17122015:135903', '5', 'tramway', '55.7805137634277', '49.2193641662598', '37', '2', '79693121778', '17122015:135919', '5', 'tramway', '55.7821998596191', '49.1111106872559', '39', '150', '79663977622', '17122015:135911', '2', 'tramway', '55.7431983947754', '49.220329284668', '26', '137', '79663977623', '17122015:135909', '4', 'tramway', '55.8203544616699', '49.1830291748047', '0', '283', '79663977625', '17122015:135721', '4', 'tramway', '55.8228721618652', '49.1651840209961', '32', '296', '79663977627', '17122015:135907', '4', 'tramway', '55.8269500732422', '49.1252288818359', '35', '272', '79663977760', '17122015:135856', '1', 'tramway', '55.8667373657227', '49.001293182373', '0', '43', '79663977626', '17122015:135915', '2', 'tramway', '55.7502288818359', '49.2090873718262', '0', '0', '79663977761', '17122015:135835', '5', 'tramway', '55.8593711853027', '49.1025924682617', '8', '330', '79663977670', '17122015:135909', '1', 'tramway', '55.8430633544922', '49.0851669311523', '23', '171', '79663977671', '17122015:135554', '6', 'tramway', '55.859317779541', '49.1024627685547', '4', '214', '79663977766', '17122015:135903', '1', 'tramway', '55.8203392028809', '49.0772285461426', '36', '190', '79663977673', '17122015:135903', '1', 'tramway', '55.8270568847656', '49.1190071105957', '32', '0', '79663977674', '17122015:135818', '5', 'tramway', '55.7600860595703', '49.1914482116699', '21', '135', '79663977769', '171220']
t3 = ['55.7741088867188', '49.1041564941406', '26', '217', '79693121800', '16122015:132849', '2', 'tramway'] 

def chunks(lst, chunk_size):
    for i in range(0, len(lst), chunk_size):
        if len(lst[i:i+chunk_size]) == chunk_size: 
            yield lst[i:i+chunk_size]

print('t1', len(t1)%8)
print('t2', len(t2)%8)
print('t3', len(t3)%8)
     
for i in chunks(t3, 8):
    print(i)
'''
from lib2to3.fixer_util import String
'''
######################################
# конвертация данных из base64 
import base64
import binascii, struct
# point
#s = "mn7Td5fgS0BSf58yxJtIQD9iIeil4EtAUn+fMsSbSEA="
#s = "oyx8NorgS0BSf5+8wZtIQGVWxHKa4EtAUn+fvMGbSEA="
s = "Q1CVKM3iS0BSf5+EF5xIQA3JGSjb4ktAUn+fhBecSEA="
#polyline
a = "sNvVpwPeS0C3YJyD3JZIQKB+Y2Dv3UtAt2Ccg9yWSECYGhpYUt5LQLdgnEefl0hAcTBgJpPeS0ARjgnyEphIQKw85/Xd3ktAEY4JwpyYSEBGjoQ8leBLQE44JgjPm0hAQiQ/mL3gS0AIxwTF95tIQNHyOw3v4EtACMcEpQicSEDEbqnCLeFLQAjHBHULnEhA08DvonDlS0BOOCaYIJxIQCqqR6/h5UtAEY4JdsebSED6HtsN0OdLQBGOCd5/mUhAgN0ws/znS0ARjgmyFZlIQFEOHmhU6EtAJxwTBGyXSECmRQNVeuhLQCccEzwWl0hAhofisrzoS0AnHBOkvZZIQDdp3w//6EtAJxwT7EiWSEAfqcbmSulLQCccE/RBlUhAsjpYWdTpS0AnHBNMRpNIQAtwSGjf6UtAJxwTzPSRSEDC6hKe3ulLQCccE0yEkUhAC3BIaN/pS0BOOCb4vo5IQMpfmxbZ6UtATjgmqAKMSEAEz3YuwulLQCccE7SLi0hAapTBapDpS0AnHBN0+YpIQFj+d/sk6UtAJxwT7PqJSEC8abV47uhLQCccEzTgiUhAXqBBa2PoS0AnHBNEwYlIQL17NQ/s5ktAJxwTDJCJSEDo3U59l+ZLQCccE4QYiUhAIseE33fmS0AnHBOsDolIQH8mLXdX5ktAJxwTZFaJSEAV3nqNHuZLQCccE2Q3ikhAnn5kDuTlS0AnHBMklItIQJ1tei+n5UtAJxwTfDuMSECj1rIJLuVLQCccEza9jEhA"

d = base64.b64decode(s)
#d = binascii.a2b_base64(a)
# данные d в bytes 
g = struct.unpack('{0}d'.format(len(d)//8), d)
print(struct.calcsize('4d'), len(d))
print(g)
'''
#######################################
'''# screenssaver
import tkinter as tk

root = tk.Tk()
root.overrideredirect(1) # убираем заголовок окна
root.state('zoomed')  # разворачиваем на весь экран

def qexit(event):
    root.destroy()

tk.Label(root,text='Hello world').place(relx=0.5,rely=0.5)


root.bind('<Button-1>',qexit) # сюда еще можно добавить события нажатия на клавиши

root.mainloop()

##################################################
#logging
import logging

logging.basicConfig(format = '%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s', level = logging.DEBUG, filename = 'mylog.log')


logging.info('текстоооо') 
logging.debug('n yyttyy hhggwsfjhoei рпшгнп ннноорфывапролдж')
'''
'''
#import egts_old
buf = b'\x01\x00\x03\x0b\x003\x00\x01\x00\x01x$\x00\x01\x00\x9d\xf12P)\x9cAb\x0b\x02\x02\x10\x1a\x00\x9cAb\x0b\x98\xb9\xc0\x9e\xfa\x94\xf8E\x81^\x01\x87\xff\xff\xff\x00\x00\x00\x00\x00\x00\x00\x11\x04\x00\x18\x0f\x03\x00\xaf\xa9'

#a = egts_old.egts_old(buf)
#c = a.header.__dict__
#print('ERROR %s' % a.error)
#print('HEADER %s' % c)
#print('SFRD %s' % a.SFRD)

import egts
print(egts.transport_header.PRV)
'''

#a = [3,9,24,66,218,15,7,111,54]
a = [[3,'1q'],[54, '2q'], [9,'3q'],[66,'4q'],[5,'5q']]
n = 4
a.sort()
def f(n,a):
    a.sort()
    b=[]
    for i,j in enumerate(a):
        b.append((abs(n-a[i][0]),j))        
    b.sort()
    return b[0][1], b[1][1]

print(f(n,a))















'''
def f(n,aa):
    a=[]
    for i in aa:
        a.append(i[0])
    if n<a[0]:
        return "n меньше всех"
    for i, j in enumerate(a):
        if n < j:
            return a[i-1]
    else:
        return "n больше всех"

def f1(n,aa):
     
    if n<aa[0][0]:
        return "n меньше всех"
    for i, j in enumerate(aa):
        if n < j[0]:
            return aa[i-1]
    else:
        return "n больше всех" 

def f2(n,aa):
    if n<aa[0][0]:
        return aa[0]
    for i, j in enumerate(aa):
        if n < j[0]:
            return aa[i-1]
    else:
        return aa[i]
        
a = [[3,'1q'],[54, '2q'], [9,'3q'],[66,'4q']]
n = 20

print(f(n,a))
print(f1(n,a))
print(f2(n,a))
'''
























