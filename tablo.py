#!/usr/bin/python
# -*- coding: utf-8 -*-

# Tablo module. Send data to board

import datetime
import binascii

class Tablo():
    def __init__(self):
        pass
    
    def setStaticString(self, strAddr, strBlock, strType, strText):
        packPrefix = 'TD'
        packCmd = '\x01'
        strPrefix = 'S'
        #strAddr = 1
        #strBlock = 1
        #strType = 4
        #strText = '���� �����'
        strEnd = '\x00'
        packEnd = '\x0D'
        b0 = bytes(packPrefix, encoding='ascii')
        b1 = bytes(packCmd, encoding='ascii')
        b2 = bytes(strPrefix, encoding='ascii')
        b3 = strAddr.to_bytes(1, byteorder='big')
        b4 = strBlock.to_bytes(1, byteorder='big')
        b5 = strType.to_bytes(1, byteorder='big')
        b6 = bytes(strText.encode('cp1251'))
        b7 = bytes(strEnd, encoding='ascii')
        b8 = (self.__xor_bytes(strAddr)^self.__xor_bytes(strBlock)^self.__xor_bytes(strType)^self.__xor_bytes(strText)).to_bytes(1, byteorder='big')
        b9 = bytes(packEnd, encoding='ascii')
        return b0+b1+b2+b3+b4+b5+b6+b7+b8+b9
    '''
    TODO: добавть отсылку данных на 2 строку
    '''
    def setDynamicString(self, strAddr, routingNum, motionDirection, arrivalTime):
        packPrefix = 'TD'
        packCmd = '\x01'
        strPrefix = 'S'
        #strAddr = 1
        strStaticBlock = 1
        strStaticType = 1
        nextBlockLoadFromSec = 5
        strStaticText = routingNum
        strStaticEnd = '\x00'
        strDynamicSpeed = 3
        strDynamicText = motionDirection
        strDynamicEnd = '\x00'
        delMsgFromMin = arrivalTime
        packEnd = '\x0D'
    
        b0 = bytes(packPrefix, encoding='ascii')
        b1 = bytes(packCmd, encoding='ascii')
        b2 = bytes(strPrefix, encoding='ascii')
        b3 = strAddr.to_bytes(1, byteorder='big')
        b4 = strStaticBlock.to_bytes(1, byteorder='big')
        b5 = strStaticType.to_bytes(1, byteorder='big')
        b6 = nextBlockLoadFromSec.to_bytes(1, byteorder='big')
        b7 = bytes(strStaticText.encode('cp1251'))
        b8 = bytes(strStaticEnd, encoding='ascii')
        b9 = strDynamicSpeed.to_bytes(1, byteorder='big')
        b10 = bytes(strDynamicText.encode('cp1251'))
        b11 = bytes(strDynamicEnd, encoding='ascii')
        b12 = delMsgFromMin.to_bytes(1, byteorder='big')
        b13 = (self.__xor_bytes(strAddr)^self.__xor_bytes(strStaticBlock)^self.__xor_bytes(strStaticType)^self.__xor_bytes(nextBlockLoadFromSec)^self.__xor_bytes(strStaticText)^self.__xor_bytes(strDynamicSpeed)^self.__xor_bytes(strDynamicText)^self.__xor_bytes(delMsgFromMin)).to_bytes(1, byteorder='big')
        b14 = bytes(packEnd, encoding='ascii')
        return b0+b1+b2+b3+b4+b5+b6+b7+b8+b9+b10+b11+b12+b13+b14
    
    def setTemperature(self, temperature):
        return bytes('TD', encoding='ascii')+int(12).to_bytes(1, byteorder='big')+bytes(temperature, encoding='ascii')+bytes('\x0D', encoding='ascii')
    
    def setTime(self):
        now_time = datetime.datetime.now()
        H = int(now_time.hour).to_bytes(1, byteorder='big')
        M = int(now_time.minute).to_bytes(1, byteorder='big')
        S = int(now_time.second).to_bytes(1, byteorder='big')
        d = int(now_time.day).to_bytes(1, byteorder='big')
        m = int(now_time.month).to_bytes(1, byteorder='big')
        y = int(now_time.strftime("%y")).to_bytes(1, byteorder='big')
        return bytes('TD', encoding='ascii')+int(8).to_bytes(1, byteorder='big')+H+M+S+d+m+y+bytes('\x0D', encoding='ascii')    

    def __xor_bytes(self, data):
        accumulator=0
        if isinstance(data, int):
            string = data.to_bytes(1, byteorder='big')
            accumulator ^= (ord(string) & 0xff)
        elif isinstance(data, str):
                data = data.encode('cp1251')
                for char in data:
                    accumulator ^= (ord(char.to_bytes(1, byteorder='big')) & 0xff)
        else:
            raise Exception('Invalid type')
        return accumulator
    
    
# data stricture of Tablo

class tablo_struct:
    def __init__(self):
        self.board = {}
   
    # [('359772039751327', 'Деревня Универсиады (С-Ж)', 'NTUuNzQxMDM2OTM0MiwgNDkuMTkyMjIyOTUyOA==', 1, 0)
    #      0                      1                                    2                            3  4
    # imei                   name                          coords ([55.2, 49.0])              owscount routedirection
        
    def __setparam_object(self, obj): 
        self.board.update({obj[0]: board_struct(obj[1],self.__decode_coords(obj[2])[0],self.__decode_coords(obj[2])[1], obj[3], obj[4])})
                
    def __decode_coords(self, coded_string):
        return binascii.a2b_base64(coded_string).decode('utf-8').split(', ')
        
        
    def set(self, data):
        for d in data:
            self.__setparam_object(d)

    def getkeys(self):
        return self.board.keys()
    
    def getitem(self,key):
        if key in self.board:
            return self.board.get(key)
        else:
            return board_struct()
        
class board_struct:
    def __init__(self, name = None, latitude = None, longitude = None, rows_count = None, route_direction = None):
            self.name = name 
            self.latitude = latitude 
            self.longitude = longitude
            self.rows_count = rows_count
            if route_direction == 1:
                self.direction = False
            elif route_direction == 0:
                self.direction = True