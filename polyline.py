'''
Created on 2.1.16

@author: nahs
# -*- coding: utf-8 -*-

'''
import binascii
from geo import geo

class routeinfo_struct:
    '''
        @note: Описание полилинии
    ''' 
    def __init__(self, description=None, name=None, reverse_name=None, route_num=None, distance=None, buffer=None):
        self.description = description
        self.name = name
        self.reverse_name = reverse_name
        self.route_num = route_num
        self.distance = distance
        self.buffer = buffer

class pl_segments_struct:
    def __init__(self,segment=None):
        self.latitude_start = segment[0]
        self.longitude_start = segment[1]
        self.latitude_end = segment[2]
        self.longitude_end = segment[3]
        self.distantion = segment[4]
        self.direction = segment[5]
        
class pl_struct:
    def __init__(self,description=None, name=None, reverse_name=None, route_num=None, distance=None, coords=None):
        self.segment = []
        self.routeinfo = routeinfo_struct(description, name, reverse_name, route_num, distance, coords[:1])
        for s in coords[2:]:
            self.segment.append(pl_segments_struct(s))


        
class polyline_struct:
    def __init__(self):
        self.pl = {}
        
    def __setparam_object(self, obj): 
        #(173, '5 Солнечный город - ЖД Вокзал', 'Железнодорожный вокзал - Railway Station', 'Солнечный город - Solnetchniy gorod', '№5', 'sNvVpwPeS0C3')
        # 0       1                                    2                                            3                                4      5
        all_distance = 0.0 
        coords = []
        g = geo()
        for i in self.__chunks(self.__decode_coords(obj[5]), 2):
            last=len(coords)
            if last == 0:
                coords.append((i[0],i[1]))
            elif last == 1:
                coords.append((0.0,0.0,i[0],i[1],0.0,0.0))
            else:
                dc = g.calculate(coords[last-1][2], coords[last-1][3], i[0],i[1])
                coords.append((coords[last-1][2], coords[last-1][3],i[0],i[1],dc['distantion'], dc['course']))
                all_distance += float(coords[last][4]) 
        self.pl.update({obj[0]: pl_struct(obj[1], obj[2], obj[3], obj[4], all_distance, coords)})
        
    def __chunks(self, obj, chunk_size):
        for i in range(0, len(obj), chunk_size):
            if len(obj[i:i+chunk_size]) == chunk_size: 
                yield obj[i:i+chunk_size]
                
    def __decode_coords(self, coded_string = None):
        return binascii.a2b_base64(coded_string).decode('utf-8').split(', ')
        #struct.unpack('{0}d'.format(len(d)//8), d)
    
        
    def set(self, data):
        for d in data:
            self.__setparam_object(d)

    def getkeys(self):
        return self.pl.keys()
    
    def getitem(self,key):
        return self.pl.get(key)
    
    
    