
from twisted.internet.protocol import ServerFactory
from twisted.protocols.basic import LineOnlyReceiver
from twisted.internet import reactor
from tablo import Tablo as tbl

class ChatProtocol(LineOnlyReceiver):
    
    name = ""
    
    def getName(self):
        if self.name != "":
            return self.name
        return self.transport.getPeer().host

    def connectionMade(self):
        print('Приконнектилсыя:', self.getName())
        self.sendLine(tbl.setTime(self))
        self.factory.clientProtocols.append(self)
        
    def connectionLost(self):
        print('Disconnect:', self.getName())
        self.factory.clientProtocols.remove(self)
        
    def lineReceived(self, line):
        print(self.getName(),' - ', line)
        
    def sendLine(self, line):
        self.transport.write(tbl.setDynamicString(1, 'N5', 'Железнодорожный вокзал - Солнечный город', 5))
        
class ChatProtocolFactory(ServerFactory):
    
    protocol = ChatProtocol
    
    def __init__(self):
        self.clientProtocols = []
        

if __name__ == "__main__":
    print('Start srv')
    factory = ChatProtocolFactory()
    reactor.listenTCP(33335, factory)
    reactor.run()


    