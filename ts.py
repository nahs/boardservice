'''
Created on 29 февр. 2016 г.

@author: nahs
'''

import datetime              

class ts:
    '''
    @note: Данные с бортового блока и бд    
    '''
    def __init__(self, 
                 latitude_start = 0.0, 
                 longitude_start = 0.0, 
                 latitude_end = 0.0, 
                 longitude_end = 0.0, 
                 speed = 0, 
                 direction = 0, 
                 time = None,
                 control = False, 
                 route = None, 
                 description = None):
        self.latitude_start = latitude_start
        self.longitude_start = longitude_start
        self.latitude_end = latitude_end
        self.longitude_end = longitude_end
        self.speed = speed
        self.direction = direction
        self.time = time
        self.newevent = control
        self.route = route
        self.description = description
                    
class ts_struct:
    '''
    @note: список транспортых средств из бд и бортовых блоков
    '''
    def __init__(self):
        self.tr = {}
    
    
    def add_move(self, data):
        '''
        @note: add new coordinate to struct
        @param data: gps data from machine
        '''
        d = data.decode('utf-8')
        b = d.split(' ')
        try:
            if int(float(b[0])) == 55:
                for i in self.__chunks(b, 8):
                    if len(i) == 8:# столько элементов в одном слове.
                        self.__set_move_object(i[:-1])
        except ValueError:
            print("ValueError")

    def __set_move_object(self, obj):
        '''
        @note: add new coordinate to struct
        @param data: gps data from machine         
                55.7425537109375 49.2279777526855 16     218   79693121804 15122015:141500 4     tramway
                latitude         longitude        speed  curse phone       time            route 
                    0                1                2      3     4           5               6     7
        '''
        for t in self.tr:
            self.tr[t].newevent = False
           
        if obj[4] in self.tr:
            x0 = self.tr[obj[4]].latitude_end
            y0 = self.tr[obj[4]].longitude_end
            self.tr.update({obj[4]: ts(latitude_start = x0, 
                                       longitude_start = y0,
                                       latitude_end = obj[0], 
                                       longitude_end = obj[1], 
                                       speed = obj[2], 
                                       direction = obj[3], 
                                       time = obj[5],
                                       control = True, 
                                       route = obj[6] 
                                    )})
                            #       ts(x0, y0, obj[0],obj[1],obj[2],obj[3],dir,obj[5],obj[6])           

    
    def __chunks(self, obj, chunk_size):
        for i in range(0, len(obj), chunk_size):
            if len(obj[i:i+chunk_size]) == chunk_size: 
                yield obj[i:i+chunk_size]
            
    def set(self, obj):
        '''
        @note: Set data from database to struct
        @param obj: result from query 
        '''
        for d in obj:
            self.__setparam_object(d)
            
    def __setparam_object(self, obj):
        '''
        @note: add data to struct
        @param obj: result from query 
                    ('+79663977761', '1300 AT-10', 55.790817, 49.099037, Decimal('0'), Decimal('0'))
                        phone      description  lastlan     lastlon      lastspeed       lastroute
                          0            1          2              3            4              5
        ''' 
        self.tr.update({obj[0][1:]: ts(latitude_end = obj[2], longitude_end = obj[3], 
                                   speed = obj[4], direction = obj[5],control = False,  description = obj[1])})
        
             
    def getkeys(self):
        return self.tr.keys()
    
    def getitem(self,key):
        return self.tr.get(key)