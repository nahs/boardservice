'''
Created on 21 дек. 2015 г.

@author: nahs
#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
import sqlalchemy as sa
import logging
from tablo import tablo_struct
from polyline import polyline_struct
from ts import ts_struct
from geo import geo

logging.basicConfig(format = '%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s', level = logging.DEBUG, filename = 'mylog.log')

class data_execute():
    '''
    @note: соединение с БД и извлечение данных:
            - список тс на маршруте
            - список табло
            - список маршрутов
    '''
    
    
    def __init__(self):
        self.board_list = None
        self.polyline_list = None
        self.ts_list = None
        self.query()
    
    def __boards(self, connection):
        # выволдим список табло
        sql = '''select B.imei, B.name, B.coordinates, B.rows_count, MR.board_direction
                    from nhs_boards as B
                    join nhs_mainroute as MR
                    on MR.board_id = B.id AND (MR.status is NULL or MR.status <> 17)
                    join nhs_route as R
                    on R.id = MR.route_id AND R.id=1 AND (R.status is NULL or R.status <> 17)
                    where B.status is NULL or B.status <> 17'''
        cursor = connection.execute(sql)
        result = cursor.fetchall()
        self.board_list = tablo_struct()
        self.board_list.set(result)
        #print('--> выгружен список табло из бд')
        logging.info('Выгружен список табло в количестве %s' % len(self.board_list.getkeys()))
        print('Выгружен список табло в количестве %s' % len(self.board_list.getkeys()))
        return cursor
    
    def __polyline(self, connection):
        #polyline list
        sql = '''select distinct MR.route_id, RL.description,  RL.course_name, RL.reverse_course_name,RL.route_num, RL.coordinates 
                    from nhs_routeline as RL
                    join nhs_mainroute as MR
                    on MR.route_line_id = RL.id AND (MR.status is NULL or MR.status <> 17)
                    join nhs_route as R
                    on R.id = MR.route_id AND (R.status is NULL or R.status <> 17)
                    where RL.status is NULL or RL.status <> 17'''
        cursor = connection.execute(sql)
        result = cursor.fetchall()
        self.polyline_list = polyline_struct()
        self.polyline_list.set(result)
        logging.info('Выгружены полилинии в количестве %s' % len(self.polyline_list.getkeys()))
        print('Выгружены полилинии в количестве %s' % len(self.polyline_list.getkeys()))
        return cursor
        #cursor = engine.execute(pl)
        #result = cursor.fetchall()
        #polyline = polyline_struct()
        #polyline.set(result)
        #print(polyline.getkeys())
        #print(polyline.getitem(172).routeinfo.distance)#выврдит общую длину маршрута
        #print(polyline.getitem(172).segment[3].distance)#выводит длину сегмента
    
    def __ts(self, connection):
        '''
        @note: Выгружает список машин в данном случае заведенных на 5 маршрут mrid=176
        TODO: Переписать на работу со всеми маршрутами. 
        @param connection: линк коннекта к ДБ
        @return: линк запрсо к ДБ для дальнейшего закрытия
        '''
        
        sql = '''select CM.carmodulephone, CM.carmoduledescription, CM.lastlan, CM.lastlon, CM.lastspeed, CM.lastcourse
                    from carmodule as CM
                    inner join caronmainroute as COMR
                    on COMR.carid = CM.carmoduleid and (COMR.Status IS NULL OR COMR.Status <> 17)
                    inner join mainroute as MR
                    on MR.id = COMR.mainrouteid AND MR.type = 3 AND MR.id = 176'''
        cursor = connection.execute(sql)
        result = cursor.fetchall()
        self.ts_list = ts_struct()
        self.ts_list.set(result)
        logging.info('Выгружены машины в количестве %s' % len(self.ts_list.getkeys()))
        print('Выгружены машины в количестве %s' % len(self.ts_list.getkeys()))
        return cursor
    
       

        
    def query(self):
        engine = sa.create_engine('mssql+pyodbc://sa:Knopo4ka2013@10.10.1.2:1433/AT?driver=SQL+Server+Native+Client+10.0', echo=False)
        connection = engine.connect()
        cursor = self.__boards(connection)
        cursor = self.__polyline(connection)
        cursor = self.__ts(connection)
        cursor.close()
        connection.close()
    
    def f(self, n,a):
        '''
        TODO: для двустрочных табло возвращать 2 ближайших тс
        @note: получаем номер сегмента ближайшего тс
        @param n:  номер сегмента принадлежащего табло
        @param a: список тс с указанием номеров егментов
        @return: искомывй семент тс 
        '''
        a.sort()
        b=[]
        for i,j in enumerate(a):
            b.append((abs(n-a[i][0]),j))        
        b.sort()
        return b[0][1]#,b[1][1]
    
    def d(self, board, ts_s):
        dist_s = board[0] - ts_s[0]
        #print('--',board,ts_s)
        i=0
        sum_s = 0
        for  i in range(dist_s):
            if dist_s != 0:                   
                sum_s += pp.getitem(plid).segment[i].distantion
            
                
        t_ts = board[1] + sum_s + ts_s[1]
        ti = t_ts / ts_s[3]
        time = ti / 60
        return time + 1 #погрешность в 1 минуту чтоб нули не выводил 
    
if __name__ == "__main__":
    b = data_execute()
    g = geo()
    board = b.board_list
    polyline = b.polyline_list
    ts = b.ts_list
    
    
    
##################################################################################### расчет расстояния от тс до табло    
    #print(pp.getkeys())
    plid = 1
    pll = polyline.getitem(plid).segment
    
    for i, imei in enumerate(polyline.getitem(plid).segment):
        #color = 'rgb(' + str(random.randint(0,255)) + ',' + str(random.randint(0,255)) +',' + str(random.randint(0,255)) +')'
        print('[',imei.longitude_start,',',imei.latitude_start,'],')
        '''
        brd = board.getitem(imei)
        
            #print('L.circle([',brd.latitude,', ',brd.longitude,'],10,{color:\'red\'}).bindPopup(\'',brd.name,',\').addTo(map);')
            s = g.own_segment(pll, [brd.latitude, brd.longitude, brd.direction], 'tablo')
            print(
                  'L.polyline([[',pll[s['segment_num']].latitude_start,',',pll[s['segment_num']].longitude_start,'],[',pll[s['segment_num']].latitude_end,',',pll[s['segment_num']].longitude_end,']],{color: \'#f00\'}).addTo(map);'
                  )
            print('L.circle([',brd.latitude,', ',brd.longitude,'],10,{color:\'#0f0\'}).bindPopup(\'',brd.name,',',brd.direction, s['f_num'],'\').addTo(map);')
            print(
                  'L.polyline([[',s['x'],',',s['y'],'],[',s['x2'],',',s['y2'],']],{color: \'#0f0\'}).addTo(map);'
                  )  
    
    
    for i, phone in enumerate(ts.getkeys()):
        tsd = ts.getitem(phone)
        
        
        s = g.own_segment(pll, [tsd.latitude_end, tsd.longitude_end, tsd.direction, tsd.speed], 'ts')
        
        if s is not None:
            if not s['course']:
                print(
                          'L.polyline([[',
                          pll[s['segment_num']].latitude_start,
                          ',',pll[s['segment_num']].longitude_start,
                          '],[',pll[s['segment_num']].latitude_end,
                          ',',pll[s['segment_num']].longitude_end,']],{color: \'#f00\'}).addTo(map);'
                          )
                
                print('L.circle([',tsd.latitude_end,', ',tsd.longitude_end,'],10,{color:\'#00f\'}).bindPopup(\'',tsd.description, tsd.direction, s['course'], s['f_num'],'\').addTo(map);')
                
                print(
                          'L.polyline([[',s['x'],',',s['y'],'],[',s['x2'],',',s['y2'],']],{color: \'#00f\'}).addTo(map);'
                          ) 
     '''       
    
    
    
    
    
    
    
    
    
    
    #173 - #5
    #print(len(pp.getitem(173).segment))
    
    #t1 = '359772039755427'
    #t3 = '359772039759098'
    #print(bb.getitem(t1).name, bb.getitem(t1).direction)
    #print(bb.getitem(t3).name, bb.getitem(t3).direction)
    
    #===========================================================================
    # for s in pp.getitem(173).segment:
    #     print('[', s.latitude_start,',', s.longitude_start,'],')
    #     print('[', s.latitude_end,',', s.longitude_end,'],')
    #     print('dista', s.distantion)
    #     print('direc %.0f'% s.direction)
    #     print('------------------')
    #===========================================================================

        
    #print('d=', pp.getitem(173).routeinfo.distance/1000, 'km')
#===============================================================================
#     c = [
#         [ 55.7453842163086 , 49.2001876831055,3 ],
#         [ 55.7404975891113 , 49.1911888122559,2 ],
#         [ 55.8270683288574 , 49.1067504882813,116 ],
#         [ 55.8268775939941 , 49.0937232971191,115 ],
#         [ 55.7722282409668 , 49.2191848754883,131 ],
#         [ 55.7801475524902 , 49.219295501709,109 ],
#         [ 55.734447479248 , 49.1803169250488,219 ],
#         [ 55.7393074035645 , 49.1891479492188,185 ],
#         [ 55.815860748291 , 49.0762977600098,46 ],
#         [ 55.8109397888184 , 49.0754890441895,47 ],
#         [ 55.8026847839355 , 49.2091827392578,225 ],
#         [ 55.8103446960449 , 49.200080871582,225 ],
#         [ 55.7774353027344 , 49.1739044189453,345 ],
#         [ 55.783447265625 , 49.1708297729492,0 ],
#         [ 55.8005180358887 , 49.0707702636719,0 ],
#         [ 55.8041152954102 , 49.0746650695801,0 ]]
# 
#     c2 = [[ 55.8271865844727 , 49.1369018554688 , 270 , 39, 5 ],
#             [ 55.8353271484375 , 49.1976928710938 , 148 , 14, 5 ],
#             [ 55.7916297912598 , 49.2194671630859 , 180 , 20, 5 ],
#             [ 55.7673721313477 , 49.2191276550293 , 226 , 28, 5 ],
#             [ 55.8006324768066 , 49.0704917907715 , 307 , 11, 5 ],
#             [ 55.8095054626465 , 49.2010536193848 , 344 , 49,5 ],
#             [ 55.7578048706055 , 49.2189788818359 , 62 , 41,5 ],
#             [ 55.740966796875 , 49.1921348571777 , 45 , 23,5 ]]
#     
#     c3 = [[ 55.8026847839355 , 49.2091827392578,225 , 14, 5 ]]
#===============================================================================
    
    
    
    
    '''
    FIXME: РАСЧЕТ ПРОГНОЗА НЕ ВЕРНЫЙ
        - ПРОВЕРИТЬ СОПОСТАВЛЕНИЕ СЕГМЕНТА И ТОЧКИ
            + ТАБЛО 
            - ТС
        - ПРОВЕРИТЬ ЛИНИЮ ТАБЛО - ТС
        - СЧИТАТЬ ТС ИСКЛЮЧИТЕЛЬНО ПЕРЕД ТАБЛО
        - ПРОВЕРЯТЬ ВСЕ ВЫВОДЫ ККОРДИНАТ НА КАРТЕ
            + ТАБЛО
            - ТС
    '''
#===============================================================================
#     ts = []
#     for cc in c3:
#         if cc[4] == 5:
#             plid = 173
#         else:
#             print('KTO ETO?')
#             break
#         t = g.own_segment(pp.getitem(plid).segment, cc)
#         if t != None:
#             ts.append(t)
# 
#     
#   
#     for i, bbb in enumerate(bb.getkeys()):
#         
#         board = g.own_segment(
#             pp.getitem(plid).segment, 
#             [    bb.getitem(bbb).latitude, 
#                  bb.getitem(bbb).longitude, 
#                  bb.getitem(bbb).direction
#             ],'tablo')
#         #print('++',board,ts)
#         if bb.getitem(bbb).rows_count == 1:
#             ts_s = f(board[0],ts)#плучаем ближайший тс
#             if board[2]:
#                 if ts_s[2]:
#                     print(
#                           '%s; %.0f' %
#                             (
#                              bb.getitem(bbb).name,
#                              d(board, ts_s)
#                             )
#                         )
#                     
#             else:
#                 if not ts_s[2]:
#                     print(
#                           '%s; %.0f' %
#                             (
#                              bb.getitem(bbb).name,
#                              d(board, ts_s)
#                             )
#                         )
#         elif bb.getitem(bbb).rows_count == 2:
#             #print(bb.getitem(bbb).name)
#             pass
#             
#        
#===============================================================================

    
    