#!/usr/bin/env python
# -*- coding: utf-8 -*-

# this server accept connect from board and every 30 second send data
'''
TODO: Объединить все в одно. рефакторить код
'''
import socket, sys, threading, random, time, logging
from tablo import Tablo
from connect_to_mssql import data_execute

logging.basicConfig(format = '%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s', level = logging.DEBUG, filename = 'mylog.log')
boards = data_execute().board_list
class Server:
    def __init__(self):
        self.host = ''
        self.port = 33335
        self.backlog = 99
        self.size = 1024
        self.server = None
        self.threads = []

    def open_socket(self):
        try:
            self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.server.bind((self.host,self.port))
            self.server.listen(self.backlog)
            logging.info('Слушаем: %s' % self.port)
        except socket.error:
            if self.server:
                self.server.close()
            logging.error('Could not open socket: %s' % sys.exc_info()[1])
            sys.exit(1)

    def run(self):
        self.open_socket()
        while True:
            try:
                cli, addr = self.server.accept()
                cli.settimeout(30.0)
            except cli as e:
                cli.close()
                logging.error('cli close %s' % e.message()) 
                 
            c = Client(cli, addr)
            c.start()
            #c.join()
            self.threads.append(c)
        print('exit', c)
        self.thread_join()
    
    def thread_join(self):
        for c in self.threads:
            logging.info('%s is joined' % c.name)
            c.join()
            
        
class Client(threading.Thread):
       
    def __init__(self, client, address):
        threading.Thread.__init__(self)
        self.tablo = Tablo()
        self.client = client
        self.address = address
        self.size = 1024
        logging.info('Установлено соединение %s %s' % address)
        logging.info('Количество активных потоков: %s' % threading.active_count())
        self.client.send(self.tablo.setTime())
        logging.info('Устновлкено время')

    def run(self):
        tbl = Tablo()
        while True:
            try:
                data = self.client.recv(self.size)
            except socket.timeout:
                logging.info('Отключено по таймауту %s %s (%s)' % (threading.currentThread().name, boards.getitem(IMEI).name, IMEI))
                self.client.close()
                break  
            if data:
                if data[:3] == b'REG':
                
                    IMEI = str(data[3:18], 'ascii')
                    pack = (self.tablo.setDynamicString(1, 'N5', 'Железнодорожный вокзал - Солнечный город', random.randint(1,5)))
                    #pack = Tablo.setTemperature('+93')
                    #pack = Tablo.setTime()
                    self.client.send(pack)
                    logging.info('Отправлена строка в потоке %s на %s (%s)' % (threading.currentThread().name, boards.getitem(IMEI).name, IMEI))        
                elif data[0] == 89:
                    logging.info('Команда принята в потоке %s на %s (%s)' % (threading.currentThread().name, boards.getitem(IMEI).name, IMEI))
                    time.sleep(60)
                    pack = (self.tablo.setDynamicString(1,'N5', 'Солнечный город - Железнодорожный вокзал', random.randint(1,5)))
                    self.client.send(pack)
                    logging.info('Отправлена строка в потоке %s на %s (%s)' % (threading.currentThread().name, boards.getitem(IMEI).name, IMEI)) 
                elif data == b'SetTemp':
                    logging.info('Установлена температура')
                elif data == b'TDSet':
                    logging.info('Установлено время')
                else:
                    logging.warning('Неизвестный ответ от %s %s %s %s ' % (threading.currentThread().name, boards.getitem(IMEI).name, IMEI, data))
                    break
            
        
if __name__ == "__main__":
    s = Server()
    s.run()


