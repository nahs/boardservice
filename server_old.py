#!/usr/bin/python
# -*- coding: utf-8 -*-

import socket, threading, datetime, random, time
from tablo import Tablo

#now_time = datetime.datetime.now() # Текущая дата со временем
class Server(threading.Thread):
    
    def __init__(self, sock, address):
        self.sock = sock
        self.addr = addr
        self.IMEI = None
        threading.Thread.__init__(self)
        print(u'Количество активных потоков: %s' % threading.activeCount())
        
    def run(self):
        while True:
            data = self.sock.recv(1024)
            self.data_recv(data)
                
    def data_recv(self, data):
        if data[:3] == b'REG':
            self.IMEI = str(data[3:18], 'ascii')
            print(self.IMEI)
            if self.IMEI[:10] == 'Call Ready':
                print(self.IMEI[:10])
            #print(u'Получено: %s : %s' % (self.addr, data))
            pack = Tablo.setDynamicString(1, 'KZN', 'Happy first Winter Day', random.randint(5,20))
            #pack = Tablo.setTemperature('+93')
            #pack = Tablo.setTime()
            self.sock.send(pack)
            print(u'Отправлена строка %s %s' % (threading.currentThread().name, self.IMEI), datetime.datetime.now())
                
        elif data[0] == 89:
            print(u'Команда принята в строке %s %s %s' % (data[1],threading.currentThread().name, self.IMEI), datetime.datetime.now())
            time.sleep(60)
            pack = Tablo.setDynamicString(1, 'KZN', 'Dooms Day is coming', random.randint(5,20))
            self.sock.send(pack)
            #print(u'Отправлено %s %s: %s' % (threading.currentThread().name, IMEI, pack))
            print(u'Отправлена строка %s %s' % (threading.currentThread().name, self.IMEI), datetime.datetime.now())
        elif data == b'SetTemp':
            print(u'Установлена температура',datetime.datetime.now())
        elif data == b'TDSet':
            print(u'Установлено время',datetime.datetime.now())
        else:
            print(u'Неизвестный ответ', data)
         #обработать сенд без ответа 

host, port = '', 33335               
threads = []
i = 0
try:
    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    soc.bind((host, port))
    soc.listen(99)
    print(u'Слушаем: %s' % port)
except socket.error as e:
    if soc:
        soc.close()
    print ("Could not open socket: %s" % e.message)

   
while True:
    i+=1
    sock, addr = soc.accept()
    sock.setblocking(0)
    print(u'Поймали: ',i , addr, datetime.datetime.now())
    t = Server(sock, addr).start()
    threads.append(t)
    
for t in threads:
    t.join()
    print('--join')
